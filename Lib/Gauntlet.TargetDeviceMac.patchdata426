    #region Remote Mac

    class RemoteMacAppInstall : IAppInstall
    {
        public string Name { get; private set; }

        public string LocalPath;

        public string WorkingDirectory;

        public string ExecutablePath;

        public string CommandArguments;

        public string ArtifactPath;
        public string BundlePath;

        public ITargetDevice Device { get; protected set; }

        public CommandUtils.ERunOptions RunOptions { get; set; }

        internal RemoteMac RemoteMac;

        public RemoteMacAppInstall(string InName, TargetDeviceRemoteMac InDevice, RemoteMac InRemoteMac)
        {
            Name = InName;
            Device = InDevice;
            CommandArguments = "";
            this.RunOptions = CommandUtils.ERunOptions.NoWaitForExit;
            RemoteMac = InRemoteMac;
        }

        public IAppInstance Run()
        {
            return Device.Run(this);
        }
    }

    public class TargetDeviceRemoteMac : ITargetDevice
    {
        /// <summary>
        /// The remote address of the macOS device. Currently ignored, as the RemoteMac class 
        /// </summary>
        internal string Address;

        internal string Username;

        internal string KeychainPassword;

        internal string UserDir;

        public TargetDeviceRemoteMac(string InAddress, string InDeviceData)
        {
            string[] CredentialsAndAddress = InAddress.Split(new[] { '@' }, 2);
            if (CredentialsAndAddress.Length == 2)
            {
                Address = CredentialsAndAddress[1];

                string[] UsernameAndKeychainPassword = CredentialsAndAddress[0].Split(new[] { ':' }, 2);
                Username = UsernameAndKeychainPassword[0];
                if (UsernameAndKeychainPassword.Length == 2)
                {
                    KeychainPassword = UsernameAndKeychainPassword[1];
                }
            }
            else
            {
                Address = CredentialsAndAddress[0];
            }

            this.RunOptions = CommandUtils.ERunOptions.NoWaitForExit;
            LocalDirectoryMappings = new Dictionary<EIntendedBaseCopyDirectory, string>();
        }

        protected Dictionary<EIntendedBaseCopyDirectory, string> LocalDirectoryMappings { get; set; }

        public string Name => Address;

        public UnrealTargetPlatform? Platform => UnrealTargetPlatform.Mac;

        public CommandUtils.ERunOptions RunOptions { get; set; }

        public bool IsAvailable => true;

        public bool IsConnected => true;

        public bool IsOn => true;

        public bool Connect()
        {
            return true;
        }

        public bool Disconnect(bool bForce = false)
        {
            return true;
        }

        public void Dispose()
        {
        }

        public Dictionary<EIntendedBaseCopyDirectory, string> GetPlatformDirectoryMappings()
        {
            throw new NotImplementedException();
        }

        protected string GetExecutableIfBundle(string InBundlePath)
        {
            if (Path.GetExtension(InBundlePath).Equals(".app", StringComparison.OrdinalIgnoreCase))
            {
                // Technically we should look at the plist, but for now...
                string BaseName = Path.GetFileNameWithoutExtension(InBundlePath);
                return Path.Combine(InBundlePath, "Contents", "MacOS", BaseName);
            }

            return InBundlePath;
        }

        public IAppInstall InstallApplication(UnrealAppConfig AppConfiguration)
        {
            var RemoteMac = new RemoteMac(AppConfiguration.ProjectFile, Address, Username);

            IBuild InBuild = AppConfiguration.Build;
            // Full path to the build
            string BuildPath;
            //  Full path to the build.app to use. This will be under BuildPath for staged builds, and the build path itself for packaged builds
            string BundlePath;
            if (InBuild is StagedBuild)
            {
                StagedBuild InStagedBuild = InBuild as StagedBuild;
                BuildPath = InStagedBuild.BuildPath;
                BundlePath = Path.Combine(BuildPath, InStagedBuild.ExecutablePath);
            }
            else
            {
                MacPackagedBuild InPackagedBuild = InBuild as MacPackagedBuild;
                BuildPath = InPackagedBuild.BuildPath;
                BundlePath = BuildPath;
            }

            // Copy the build to the remote Mac.
            Log.Info("[Remote] Uploading {0} to Mac...", BuildPath);
            RemoteMac.UploadDirectory(new DirectoryReference(BuildPath));

            UserDir = Path.Combine(BuildPath, "GauntletTemp");
            Directory.CreateDirectory(UserDir);

            RemoteMacAppInstall AppInstall = new RemoteMacAppInstall(AppConfiguration.Name, this, RemoteMac);
            AppInstall.BundlePath = BundlePath;
            AppInstall.LocalPath = RemoteMac.GetRemotePath(BuildPath);
            AppInstall.WorkingDirectory = AppInstall.LocalPath;
            AppInstall.RunOptions = RunOptions;
            AppInstall.ExecutablePath = GetExecutableIfBundle(BundlePath);
            AppInstall.CommandArguments = Regex.Replace(AppConfiguration.CommandLine, @"\$\(InstallPath\)", RemoteMac.GetRemotePath(BuildPath), RegexOptions.IgnoreCase);
            AppInstall.CommandArguments += string.Format(" -userdir={0}", RemoteMac.EscapeShellArgument(RemoteMac.GetRemotePath(UserDir)));
            AppInstall.ArtifactPath = Path.Combine(UserDir, @"Saved");
            Directory.CreateDirectory(AppInstall.ArtifactPath);

            RemoteMac.Execute(new DirectoryReference(@"C:\"), string.Format("mkdir {0}", RemoteMac.EscapeShellArgument(RemoteMac.GetRemotePath(UserDir))));
            RemoteMac.Execute(new DirectoryReference(UserDir), "rm -rf Saved && mkdir Saved");

            if (LocalDirectoryMappings.Count == 0)
            {
                PopulateDirectoryMappings(BuildPath);
            }

            return AppInstall;
        }

        public void PopulateDirectoryMappings(string ProjectDir)
        {
            LocalDirectoryMappings.Add(EIntendedBaseCopyDirectory.Build, Path.Combine(ProjectDir, "Build"));
            LocalDirectoryMappings.Add(EIntendedBaseCopyDirectory.Binaries, Path.Combine(ProjectDir, "Binaries"));
            LocalDirectoryMappings.Add(EIntendedBaseCopyDirectory.Config, Path.Combine(ProjectDir, "Saved", "Config"));
            LocalDirectoryMappings.Add(EIntendedBaseCopyDirectory.Content, Path.Combine(ProjectDir, "Content"));
            LocalDirectoryMappings.Add(EIntendedBaseCopyDirectory.Demos, Path.Combine(ProjectDir, "Saved", "Demos"));
            LocalDirectoryMappings.Add(EIntendedBaseCopyDirectory.Profiling, Path.Combine(ProjectDir, "Saved", "Profiling"));
            LocalDirectoryMappings.Add(EIntendedBaseCopyDirectory.Saved, Path.Combine(UserDir, "Saved"));
        }

        public bool PowerOff()
        {
            return true;
        }

        public bool PowerOn()
        {
            return true;
        }

        public bool Reboot()
        {
            return true;
        }

        public IAppInstance Run(IAppInstall App)
        {
            RemoteMacAppInstall MacInstall = App as RemoteMacAppInstall;

            if (MacInstall == null)
            {
                throw new AutomationException("Invalid install type!");
            }

            Log.Info("Launching {0} on {1}", App.Name, ToString());
            Log.Verbose("\t{0}", MacInstall.CommandArguments);

            return new RemoteMacAppInstance(MacInstall);
        }
    }

    class RemoteMacAppInstance : IAppInstance
    {
        private Thread RemoteExecution;

        private RemoteMacAppInstall MacInstall;

        public RemoteMacAppInstance(RemoteMacAppInstall InMacInstall)
        {
            MacInstall = InMacInstall;

            HasExited = false;
            StdOut = string.Empty;
            ExitCode = -1;
            WasKilled = false;

            RemoteExecution = new Thread(ExecuteMacAppOnBackgroundThread);
            RemoteExecution.Start();
        }

        private void ExecuteMacAppOnBackgroundThread()
        {
            try
            {
                TargetDeviceRemoteMac TargetDevice = MacInstall.Device as TargetDeviceRemoteMac;

                // Determine the shell, so we know how to format the command.
                StringBuilder Shell;
                MacInstall.RemoteMac.ExecuteAndCaptureOutput("echo $0", out Shell);
                bool bIsZsh = false;
                if (Shell.ToString().Contains("zsh"))
                {
                    Log.Info("Detected zsh as the shell on the macOS machine.");
                    bIsZsh = true;
                }
                else if (Shell.ToString().Contains("bash"))
                {
                    Log.Info("Detected bash as the shell on the macOS machine.");
                }
                else
                {
                    Log.Error("Unsupported shell on remote macOS machine: " + Shell.ToString().Trim());
                }

                Log.Info("Launching macOS application at path: " + MacInstall.RemoteMac.GetRemotePath(MacInstall.BundlePath));

                string FullCommand;
                string NoGlob = "set -o noglob && ";
                if (bIsZsh)
                {
                    NoGlob = "noglob ";
                }
                if (!string.IsNullOrWhiteSpace(TargetDevice.KeychainPassword))
                {
                    // Some apps require access to the local keychain, so allow them to have it unlocked before they execute by setting the password into the DeviceList.json file.
                    FullCommand = String.Format(
                        "cd {0} && security unlock-keychain -p '{2}' ~/Library/Keychains/login.keychain-db && {3}{1}",
                        RemoteMac.EscapeShellArgument(MacInstall.RemoteMac.GetRemotePath(MacInstall.BundlePath)),
                        MacInstall.RemoteMac.GetRemotePath(MacInstall.ExecutablePath) + " " + MacInstall.CommandArguments,
                        TargetDevice.KeychainPassword,
                        NoGlob);
                }
                else
                {
                    FullCommand = String.Format(
                        "cd {0} && {2}{1}",
                        RemoteMac.EscapeShellArgument(MacInstall.RemoteMac.GetRemotePath(MacInstall.BundlePath)),
                        MacInstall.RemoteMac.GetRemotePath(MacInstall.ExecutablePath) + " " + MacInstall.CommandArguments,
                        NoGlob);
                }
                ExitCode = MacInstall.RemoteMac.ExecuteAndCaptureOutput(FullCommand, (line) =>
                {
                    if (StdOut == string.Empty)
                    {
                        StdOut = line;
                    }
                    else
                    {
                        StdOut += "\n" + line;
                    }
                });
                HasExited = true;

                DownloadSavedFolder();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                ExitCode = -1;
                HasExited = true;

                DownloadSavedFolder();
            }
        }

        public bool HasExited { get; private set; }

        public string StdOut { get; private set; }

        public int ExitCode { get; private set; }

        public bool WasKilled { get; private set; }

        public string CommandLine => MacInstall.ExecutablePath + " " + MacInstall.CommandArguments;

        public string ArtifactPath
        {
            get
            {
                Log.Info("Artifact path: {0}", MacInstall.ArtifactPath);
                return MacInstall.ArtifactPath;
            }
        }

        public ITargetDevice Device => MacInstall.Device;

        public void Kill()
        {
            Log.Info(Environment.StackTrace);

            MacInstall.RemoteMac.Execute(new DirectoryReference(@"C:\"), string.Format("killall -9 {0}", RemoteMac.EscapeShellArgument(Path.GetFileName(MacInstall.BundlePath))));
            MacInstall.RemoteMac.Execute(new DirectoryReference(@"C:\"), string.Format("osascript -e 'quit app {0}'", RemoteMac.EscapeShellArgument(MacInstall.RemoteMac.GetRemotePath(MacInstall.BundlePath))));
            WasKilled = true;
            ExitCode = 1;
            RemoteExecution.Abort();

            DownloadSavedFolder();

            HasExited = true;
        }

        public int WaitForExit()
        {
            RemoteExecution.Join();
            return ExitCode;
        }

        private object DownloadLock = new object();

        private void DownloadSavedFolder()
        {
            lock (DownloadLock)
            {
                MacInstall.RemoteMac.DownloadDirectory(new DirectoryReference((MacInstall.Device as TargetDeviceRemoteMac).UserDir));
            }
        }
    }

    #endregion