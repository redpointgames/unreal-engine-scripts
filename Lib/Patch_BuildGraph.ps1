param(
    # The engine to patch.
    [Parameter(Mandatory = $true)][string] $Engine
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Internal_SetupEnvironment.ps1

$EnginePath = Resolve-EnginePath $Engine

# Mark engine files as read-write.
Get-ChildItem -Path "$EnginePath\Engine\Source\Programs" -Recurse -Filter "*" | % {
    if ($_.IsReadOnly) {
        $_.IsReadOnly = $false
    }
}

# Patch the BuildGraph so that it uses a root directory from the environment for temporary storage, rather
# than assuming uebp_LOCAL_ROOT.
$BuildGraphPath = "$EnginePath\Engine\Source\Programs\AutomationTool\BuildGraph\BuildGraph.cs"
if (Test-Path $BuildGraphPath) {
    $BuildGraphScript = Get-Content -Raw -Path $BuildGraphPath
    if ($BuildGraphScript.Contains("DirectoryReference RootDir = new DirectoryReference(CommandUtils.CmdEnv.LocalRoot);")) {
        $BuildGraphScript = $BuildGraphScript.Replace(
            "DirectoryReference RootDir = new DirectoryReference(CommandUtils.CmdEnv.LocalRoot);",
            "DirectoryReference RootDir = new DirectoryReference(Environment.GetEnvironmentVariable(`"BUILD_GRAPH_PROJECT_ROOT`") ?? CommandUtils.CmdEnv.LocalRoot);"
        )
        Set-RetryableContent -Path $BuildGraphPath -Value $BuildGraphScript
    }
    if ($BuildGraphScript.Contains("if(!ModifiedFiles.ContainsKey(File.RelativePath) && !File.Compare(CommandUtils.RootDirectory, out Message))")) {
        $BuildGraphScript = $BuildGraphScript.Replace(
            "if(!ModifiedFiles.ContainsKey(File.RelativePath) && !File.Compare(CommandUtils.RootDirectory, out Message))",
            "if (!ModifiedFiles.ContainsKey(File.RelativePath) && !File.Compare(Environment.GetEnvironmentVariable(`"BUILD_GRAPH_PROJECT_ROOT`") != null ? new DirectoryReference(Environment.GetEnvironmentVariable(`"BUILD_GRAPH_PROJECT_ROOT`")) : CommandUtils.RootDirectory, out Message))"
        )
        Set-RetryableContent -Path $BuildGraphPath -Value $BuildGraphScript
    }
    if ($BuildGraphScript.Contains("if (!ModifiedFiles.ContainsKey(File.RelativePath) && !File.Compare(CommandUtils.RootDirectory, out Message))")) {
        $BuildGraphScript = $BuildGraphScript.Replace(
            "if (!ModifiedFiles.ContainsKey(File.RelativePath) && !File.Compare(CommandUtils.RootDirectory, out Message))",
            "if (!ModifiedFiles.ContainsKey(File.RelativePath) && !File.Compare(Environment.GetEnvironmentVariable(`"BUILD_GRAPH_PROJECT_ROOT`") != null ? new DirectoryReference(Environment.GetEnvironmentVariable(`"BUILD_GRAPH_PROJECT_ROOT`")) : CommandUtils.RootDirectory, out Message))"
        )
        Set-RetryableContent -Path $BuildGraphPath -Value $BuildGraphScript
    }
    if ($BuildGraphScript.Contains("if (ModifiedFiles.Count > 0)")) {
        $BuildGraphScript = $BuildGraphScript.Replace(
            "if (ModifiedFiles.Count > 0)",
            "if (ModifiedFiles.Count > 0 && Environment.GetEnvironmentVariable(`"BUILD_GRAPH_ALLOW_MUTATION`") != `"true`")"
        )
        Set-RetryableContent -Path $BuildGraphPath -Value $BuildGraphScript
    }
    if ($BuildGraphScript.Contains("if(ModifiedFiles.Count > 0)")) {
        $BuildGraphScript = $BuildGraphScript.Replace(
            "if(ModifiedFiles.Count > 0)",
            "if(ModifiedFiles.Count > 0 && Environment.GetEnvironmentVariable(`"BUILD_GRAPH_ALLOW_MUTATION`") != `"true`")"
        )
        Set-RetryableContent -Path $BuildGraphPath -Value $BuildGraphScript
    }
    if ($BuildGraphScript.Contains("FileReference FullScriptFile = FileReference.Combine(Unreal.RootDirectory, ScriptFileName);")) {
        $BuildGraphScript = $BuildGraphScript.Replace(
            "FileReference FullScriptFile = FileReference.Combine(Unreal.RootDirectory, ScriptFileName);",
            "FileReference FullScriptFile = new FileReference(ScriptFileName);"
        )
        Set-RetryableContent -Path $BuildGraphPath -Value $BuildGraphScript
    }
    if ($BuildGraphScript.Contains("if (!FullScriptFile.IsUnderDirectory(Unreal.RootDirectory))")) {
        $StartIndex = $BuildGraphScript.IndexOf("if (!FullScriptFile.IsUnderDirectory(Unreal.RootDirectory))");
        $EndIndex = $BuildGraphScript.IndexOf("ScriptFileName = FullScriptFile.MakeRelativeTo(Unreal.RootDirectory).Replace('\\', '/');")
        $BuildGraphScript = $BuildGraphScript.Substring(0, $StartIndex) + $BuildGraphScript.Substring($EndIndex)
        Set-RetryableContent -Path $BuildGraphPath -Value $BuildGraphScript
    }
    if ($BuildGraphScript.Contains("ScriptFileName = FullScriptFile.MakeRelativeTo(Unreal.RootDirectory).Replace('\\', '/');")) {
        $BuildGraphScript = $BuildGraphScript.Replace(
            "ScriptFileName = FullScriptFile.MakeRelativeTo(Unreal.RootDirectory).Replace('\\', '/');",
            ""
        )
        Set-RetryableContent -Path $BuildGraphPath -Value $BuildGraphScript
    }
}
$TempStoragePath = "$EnginePath\Engine\Source\Programs\AutomationTool\BuildGraph\TempStorage.cs"
if (Test-Path $TempStoragePath) {
    $TempStorageScript = Get-Content -Raw -Path $TempStoragePath
    if ($TempStorageScript.Contains("if(RequireMatchingTimestamps() && !TempStorage.IsDuplicateBuildProduct(LocalFile))")) {
        $TempStorageScript = $TempStorageScript.Replace(
            "if(RequireMatchingTimestamps() && !TempStorage.IsDuplicateBuildProduct(LocalFile))",
            "if(RequireMatchingTimestamps() && !TempStorage.IsDuplicateBuildProduct(LocalFile) && Environment.GetEnvironmentVariable(`"BUILD_GRAPH_ALLOW_MUTATION`") != `"true`")"
        )
        Set-RetryableContent -Path $TempStoragePath -Value $TempStorageScript
    }
    if ($TempStorageScript.Contains("if (RequireMatchingTimestamps() && !TempStorage.IsDuplicateBuildProduct(LocalFile))")) {
        $TempStorageScript = $TempStorageScript.Replace(
            "if (RequireMatchingTimestamps() && !TempStorage.IsDuplicateBuildProduct(LocalFile))",
            "if (RequireMatchingTimestamps() && !TempStorage.IsDuplicateBuildProduct(LocalFile) && Environment.GetEnvironmentVariable(`"BUILD_GRAPH_ALLOW_MUTATION`") != `"true`")"
        )
        Set-RetryableContent -Path $TempStoragePath -Value $TempStorageScript
    }
    # Workaround a weird bug that only impacts engine builds on 4.27.2 later, where the build graph dependencies don't seem to be
    # specified correctly for UE4Server and UnrealServer targets.
    if ($TempStorageScript.Contains("bool bRemote = SharedDir != null && bPushToRemote && bWriteToSharedStorage;")) {
        $TempStorageScript = $TempStorageScript.Replace(
            "bool bRemote = SharedDir != null && bPushToRemote && bWriteToSharedStorage;",
            "bool bRemote = SharedDir != null && (bPushToRemote || NodeName.StartsWith(`"Compile UE4Server`") || NodeName.StartsWith(`"Compile UnrealServer`")) && bWriteToSharedStorage;"
        )
        Set-RetryableContent -Path $TempStoragePath -Value $TempStorageScript
    }
    # Upgrade already patched BuildGraph to include UnrealServer as well.
    if ($TempStorageScript.Contains("bool bRemote = SharedDir != null && (bPushToRemote || NodeName.StartsWith(`"Compile UE4Server`")) && bWriteToSharedStorage;")) {
        $TempStorageScript = $TempStorageScript.Replace(
            "bool bRemote = SharedDir != null && (bPushToRemote || NodeName.StartsWith(`"Compile UE4Server`")) && bWriteToSharedStorage;",
            "bool bRemote = SharedDir != null && (bPushToRemote || NodeName.StartsWith(`"Compile UE4Server`") || NodeName.StartsWith(`"Compile UnrealServer`")) && bWriteToSharedStorage;"
        )
        Set-RetryableContent -Path $TempStoragePath -Value $TempStorageScript
    }
}
$BgScriptReaderPath = "$EnginePath\Engine\Source\Programs\Shared\EpicGames.BuildGraph\BgScriptReader.cs"
if (Test-Path $BgScriptReaderPath) {
    $BgScriptReader = Get-Content -Raw -Path $BgScriptReaderPath
    # Workaround <Switch> block being broken in Unreal Engine 5.
    $LE = "`n"
    if ($BgScriptReader.Contains("`r`n")) {
        $LE = "`r`n"
    }
    if ($BgScriptReader.Contains("if (ChildElement.Name == `"Default`" || await EvaluateConditionAsync(ChildElement))$LE`t`t`t`t{$LE`t`t`t`t`tawait ReadContentsAsync(ChildElement);")) {
        $BgScriptReader = $BgScriptReader.Replace(
            "if (ChildElement.Name == `"Default`" || await EvaluateConditionAsync(ChildElement))$LE`t`t`t`t{$LE`t`t`t`t`tawait ReadContentsAsync(ChildElement);",
            "if (ChildElement.Name == `"Default`" || await EvaluateConditionAsync(ChildElement))$LE`t`t`t`t{$LE`t`t`t`t`tawait ReadContentsAsync(ChildElement);$LE`t`t`t`t`tbreak;"
        )
        Set-RetryableContent -Path $BgScriptReaderPath -Value $BgScriptReader
    }
}


# If this is Unreal Engine 5, we need to copy across fastJSON if it doesn't exist.
$IsUnrealEngine5 = $false
if ((Get-Content -Raw "$EnginePath\Engine\Source\Programs\AutomationTool\BuildGraph\BuildGraph.Automation.csproj").Contains("netcoreapp")) {
    Write-Host "Detected Unreal Engine 5..."
    if (!(Test-Path "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0")) {
        New-Item -ItemType Directory "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0"
    }
    if (!(Test-Path "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0\fastJSON.dll")) {
        Copy-Item -Force "$PSScriptRoot\UE5\fastJSON.dll" "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0\fastJSON.dll"
    }
    if (!(Test-Path "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0\fastJSON.deps.json")) {
        Copy-Item -Force "$PSScriptRoot\UE5\fastJSON.deps.json" "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0\fastJSON.deps.json"
    }
    if (!(Test-Path "$EnginePath\Engine\Binaries\DotNET")) {
        New-Item -ItemType Directory "$EnginePath\Engine\Binaries\DotNET"
    }
    if (!(Test-Path "$EnginePath\Engine\Binaries\DotNET\Ionic.Zip.Reduced.dll")) {
        Copy-Item -Force "$PSScriptRoot\UE5\Ionic.Zip.Reduced.dll" "$EnginePath\Engine\Binaries\DotNET\Ionic.Zip.Reduced.dll"
    }
    if (!(Test-Path "$EnginePath\Engine\Binaries\DotNET\OneSky.dll")) {
        Copy-Item -Force "$PSScriptRoot\UE5\OneSky.dll" "$EnginePath\Engine\Binaries\DotNET\OneSky.dll"
    }
    $IsUnrealEngine5 = $true
}

# Now rebuild BuildGraph.
$AutomationToolProject = "$EnginePath\Engine\Source\Programs\AutomationTool\BuildGraph\BuildGraph.Automation.csproj"
if ($global:IsMacOS -or $global:IsLinux) {
    $AutomationToolProject = $AutomationToolProject.Replace("\", "/")
}
$MSBuildPath = Get-MSBuildPath
if ($MSBuildPath -eq $null) {
    Write-Host "Unable to locate MSBuild!"
    exit 1
}
if ($IsUnrealEngine5) {
    Get-ChildItem -Path "$EnginePath\Engine\Source\Programs\Shared\EpicGames.Core" -Recurse -Filter "*" | % {
        if ($_.IsReadOnly) {
            $_.IsReadOnly = $false
        }
    }

    if (Test-Path "$env:ProgramFiles\dotnet\dotnet.exe") {
        $Sources = (& "$env:ProgramFiles\dotnet\dotnet.exe" nuget list source | Out-String)
        if (!$Sources.Contains("https://api.nuget.org/v3/index.json")) {
            # Make sure we have the NuGet source for restore.
            & "$env:ProgramFiles\dotnet\dotnet.exe" nuget add source -n nuget.org "https://api.nuget.org/v3/index.json"
        }
    }
    
    Write-Host "Restoring packages..."
    & "$MSBuildPath" "/nologo" "/verbosity:quiet" "$AutomationToolProject" "/property:Configuration=Development" "/property:Platform=AnyCPU" "/p:WarningLevel=0" "/target:Restore"
    if ($LastExitCode -ne 0) {
        exit $LastExitCode
    }
}
& "$MSBuildPath" "/nologo" "/verbosity:quiet" "$AutomationToolProject" "/property:Configuration=Development" "/property:Platform=AnyCPU" "/p:WarningLevel=0"
if ($LastExitCode -ne 0) {
    exit $LastExitCode
}
exit 0