param([bool][switch] $CheckOnly, [string]$OverridePath)

. $PSScriptRoot\Internal_SetupEnvironment.ps1

$BuildConfig = Get-BuildConfig
if (Get-IsEngine) {
    Write-Error "Format.ps1 can not be used when targeting Unreal Engine itself."
    exit 1
}

$ClangFormat = Get-ClangFormat
if ($ClangFormat -eq $null) {
    Write-Error "clang-format.exe could not be found. Ensure it is on the PATH, or install the Clang/LLVM components in the Visual Studio Installer."
    exit 1
}

$TargetDirectories = @()
if ($OverridePath -ne $null -and $OverridePath -ne "") {
    $TargetDirectories = @((Get-Item -Path $OverridePath).FullName)
} elseif (Get-IsProject) {
    $TargetDirectories = $BuildConfig.Distributions | % { "$ProjectRoot\$($_.FolderName)" }
} else {
    $TargetDirectories = @("$ProjectRoot\$($BuildConfig.PluginName)")
}

$Role = "Formatting"
if ($CheckOnly) {
    $Role = "Checking"
}
Write-Output "$Role source code with clang-format..."
Set-Content -Path "$ProjectRoot\.clang-format" -Value "$(Get-Content -Raw -Path "$PSScriptRoot\.clang-format")"
$FormatJobs = @()
foreach ($TargetDirectory in $TargetDirectories) {
    $FormatJobs += Start-Job -ArgumentList @($TargetDirectory, $ClangFormat, $CheckOnly, $Role, ($OverridePath -ne $null -and $OverridePath -ne "")) -ScriptBlock {
        param([string] $TargetDirectory, [string] $ClangFormat, [bool] $CheckOnly, [string] $Role, [bool] $IsOverride)
        function Batch-Files {
            begin {
                $ObjectList = @()
            }
            process {
                if ($ObjectList.Length -lt 20) {
                    $ObjectList += $_.FullName
                } else {
                    @{
                        Batch = $ObjectList
                    }
                    $ObjectList = @()
                }
            }
            end {
                if ($ObjectList.Length -gt 0) {
                    @{
                        Batch = $ObjectList
                    }
                    $ObjectList = @()
                }
            }
        }
        Write-Output "$Role '$TargetDirectory': Started..."
        $Stopwatch = [System.Diagnostics.StopWatch]::StartNew()
        $TargetDirectorySource = "$TargetDirectory\Source"
        if ($IsOverride) {
            $TargetDirectorySource = "$TargetDirectory"
        }
        $ExitCodes = Get-ChildItem -Recurse -File -Path "$TargetDirectorySource" | Where-Object { 
            $_.FullName.EndsWith(".cpp") -or $_.FullName.EndsWith(".h")
        } | Batch-Files | % {
            if ($CheckOnly) {
                & "$ClangFormat" --style=file -i -n $($_.Batch)
            } else {
                & "$ClangFormat" --style=file -i $($_.Batch)
            }
            $LastExitCode
        }
        $Stopwatch.Stop()
        if ($CheckOnly) {
            $AnyFailed = $false
            foreach ($ExitCode in $ExitCodes) {
                if ($ExitCode -ne 0) {
                    $AnyFailed = $true
                    break
                }
            }
            if ($AnyFailed) {
                Write-Error "$Role '$TargetDirectory': Failed in $([int][System.Math]::Ceiling($Stopwatch.Elapsed.TotalSeconds)) secs. Please run Format.ps1 to comply with the code formatting standards in this repository."
                exit 1
            } else {
                Write-Output "$Role '$TargetDirectory': Success in $([int][System.Math]::Ceiling($Stopwatch.Elapsed.TotalSeconds)) secs"
            }
        } else {
            Write-Output "$Role '$TargetDirectory': Finished in $([int][System.Math]::Ceiling($Stopwatch.Elapsed.TotalSeconds)) secs"
        }        
    }
}
Receive-Job -Job $FormatJobs -Wait -AutoRemoveJob
exit 0