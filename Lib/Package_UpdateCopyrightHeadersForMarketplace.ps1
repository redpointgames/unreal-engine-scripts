param(
    [string] $Path,
    [string] $CopyrightHeader,
    [string] $CopyrightExcludes
)

$ErrorActionPreference = "Stop"

Write-Output "Updating copyright header to: $CopyrightHeader"

$ExcludePaths = $CopyrightExcludes.Split(";") | ForEach-Object {
    [System.IO.Path]::Combine($Path, $_).Replace("/", [System.IO.Path]::DirectorySeparatorChar).Replace("\", [System.IO.Path]::DirectorySeparatorChar)
}

function Get-IsFileExcluded($TargetPath) {
    foreach ($ExcludePath in $ExcludePaths) {
        if ($TargetPath.StartsWith($ExcludePath)) {
            return $true;
        }
    }
    return $false;
}

foreach ($File in (Get-ChildItem -Recurse -Path $Path | Where-Object { 
    $_ -ne $null -and
    (
        $_.Name.EndsWith(".h") -or 
        $_.Name.EndsWith(".hpp") -or 
        $_.Name.EndsWith(".cs") -or 
        $_.Name.EndsWith(".c") -or 
        $_.Name.EndsWith(".cpp")
    ) -and !(
        $_.FullName.Substring($Path.Length).Contains("Binaries") -or 
        $_.FullName.Substring($Path.Length).Contains("Intermediate") -or 
        $_.FullName.Substring($Path.Length).Contains("BuildScripts") -or 
        $_.FullName.Substring($Path.Length).Contains("Plugins") -or
        (Get-IsFileExcluded $_.FullName)
    ) }))
{
    $Name = $File.Name
    Write-Output "Updating $Name..."

    $RawContent = Get-Content $File.FullName -Raw
    if ($null -eq $RawContent) {
        continue
    }
    $Content = [System.Linq.Enumerable]::ToList(($RawContent).Split("`n"))
    while ($Content.Count -gt 0 -and ($Content[0].StartsWith("// ") -or [System.String]::IsNullOrWhiteSpace($Content[0]))) {
        $Content.RemoveAt(0) | Out-Null
    }
    $Content.Insert(0, "");
    $Content.Insert(0, "// $CopyrightHeader");
    while ($Content.Count -gt 0 -and [System.String]::IsNullOrWhiteSpace($Content[$Content.Count - 1])) {
        $Content.RemoveAt($Content.Count - 1) | Out-Null
    }
    for ($i = 0; $i -lt $Content.Count; $i++) {
        $Content[$i] = $Content[$i].TrimEnd()
    }
    $JoinedContent = [System.String]::Join("`n", $Content)
    Set-Content -Path $File.FullName -Value $JoinedContent -NoNewLine
}