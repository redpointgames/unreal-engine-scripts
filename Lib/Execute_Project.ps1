param(
    # The engine you are targeting. Can be a version number or an absolute path.
    [Parameter(Mandatory=$true)][string] $Engine,
    # The distribution name, as it appears in BuildConfig.json.
    [Parameter(Mandatory=$true)][string] $Distribution,
    # If true, executes the build step in the build graph.
    [switch][bool]$ExecuteBuild,
    # If true, executes the test steps in the build graph.
    [switch][bool]$ExecuteTests,
    # If true, executes the deployment steps in the build graph.
    [switch][bool]$ExecuteDeployment,
    # If true, turns on IWYU and -StrictIncludes. This is much more time consuming, but required to pass Marketplace submission.
    [switch][bool] $StrictIncludes,
    # If true, the working directory will be mapped as a drive on Windows to reduce the length of file paths. Engine builds
    # always map drives, but you can optionally enable this for project or plugin builds.
    [switch][bool] $MapDriveForShorterPaths
)

. $PSScriptRoot\Internal_SetupEnvironment.ps1

Map-WorkingDirectory -MapDriveForShorterPaths:$MapDriveForShorterPaths -Path (Get-Location).Path -ScriptBlock {
    param([string] $Path)

    Push-Location $Path
    try {
        $BuildConfig = Get-BuildConfig
        $EnginePath = Resolve-EnginePath $Engine
        $DistributionConfig = Get-Distribution $Distribution

        $local:BasePath = (Get-Location).Path
        $local:BasePath = ($local:BasePath.TrimEnd("\") + "\")

        # Set up common variables.
        $ProjectName = $DistributionConfig.ProjectName
        $FolderName = $DistributionConfig.FolderName

        # Compute BuildGraph settings.
        . "$local:BasePath$global:RelativePSScriptRoot\BuildGraphSettings_Project.ps1"
        $Settings = Get-BuildGraphSettingsForProject $ProjectName $FolderName $DistributionConfig $EnginePath $StrictIncludes $ExecuteBuild $ExecuteTests $ExecuteDeployment ""
        
        # Patch BuildGraph.
        & "$local:BasePath$global:RelativePSScriptRoot\Patch_BuildGraph.ps1" -Engine "$Engine"
        if ($LastExitCode -ne 0) {
            exit $LastExitCode
        }

        # Patch Gauntlet if we need to.
        if ($ExecuteTests) {
            if ($null -ne $local:DistributionConfig.Tests) {
                foreach ($Test in $local:DistributionConfig.Tests) {
                    if ($Test.Type -eq "Gauntlet") {
                        & $PSScriptRoot\Patch_Gauntlet.ps1 -Engine "$Engine" -Distribution "$Distribution" -TestName $Test.Name
                        if ($LastExitCode -ne 0) { exit $LastExitCode }

                        # TODO: We have to combine all of the Gauntlet test scripts together so we can support multiple Gauntlet tests when
                        # running from the command line like this outside BuildGraph.
                        break
                    }
                }
            }
        }

        # Run any relevant preparation scripts.
        if ($null -ne $DistributionConfig.Prepare) {
            foreach ($Prepare in $DistributionConfig.Prepare) {
                if ($local:Prepare.Type -eq "Custom") {
                    if ($null -ne $local:Prepare.RunBefore -and $local:Prepare.RunBefore.Contains("BuildGraph")) {
                        powershell.exe -ExecutionPolicy Bypass "$((Get-Location).Path)\$($local:Prepare.ScriptPath.Replace("/", "\"))"
                        if ($LastExitCode -ne 0) { exit $LastExitCode }
                    }
                }
            }
        }

        # BuildGraph in Unreal Engine 5.0 causes input files to be unnecessarily modified. Just allow mutation since I'm not sure what the bug is.
        $env:BUILD_GRAPH_ALLOW_MUTATION="true"
        
        # Execute BuildGraph.
        if (!(Test-Path "$local:BasePath$FolderName\Saved")) {
            New-Item -ItemType Directory -Path "$local:BasePath$FolderName\Saved"
        }
        $BuildGraphArgsArray = @()
        $Settings.Keys | % {
            $BuildGraphArgsArray += "-set:$_=`"$($Settings.Item($_).Replace("__REPOSITORY_ROOT__", "$local:BasePath").Replace("/", "\").TrimEnd("\"))`""
        }
        $env:uebp_LOCAL_ROOT = $EnginePath
        $env:BUILD_GRAPH_PROJECT_ROOT = $local:BasePath
        if ($ExecuteTests) {
            # Permit Gauntlet tests to modify files.
            $env:BUILD_GRAPH_ALLOW_MUTATION = "true"
        }
        taskkill /f /t /im AutomationToolLauncher.exe
        # note: We don't yet have this retry logic on BuildGraph invocations that happen
        # as part of a GitLab build, but I haven't seen PCH memory errors occur there. Port
        # this logic across if we see these errors happening there as well.
        foreach ($Value in $BuildGraphArgsArray) {
            Write-Host $Value
        }
        while ($true) {
            & "$PSScriptRoot\Internal_RunUAT.ps1" -UATEnginePath "$EnginePath" BuildGraph -Script="$local:BasePath$global:RelativePSScriptRoot\BuildGraph_Project.xml" -Target="End" "-set:EnginePath=`"$EnginePath`"" $BuildGraphArgsArray | Tee-Object -FilePath "$local:BasePathBuildScripts\Temp\Build.log"
            if ($LASTEXITCODE -ne 0) {
                $BuildLogLines = (Get-Content "$local:BasePathBuildScripts\Temp\Build.log")
                if ($BuildLogLines -is [string]) {
                    $BuildLogLines = @($BuildLogLines)
                }
                $PCHRetryOn = $true
                $NeedsRetry = $false
                for ($i = 0; $i -lt $BuildLogLines.Length; $i++) {
                    if ($BuildLogLines[$i].Contains("error C3859") -and $PCHRetryOn) {
                        $NeedsRetry = $true
                        break
                    }
                    if ($BuildLogLines[$i].Contains("@buildgraph PCH-RETRY-ON")) {
                        $PCHRetryOn = $true
                    }
                    if ($BuildLogLines[$i].Contains("@buildgraph PCH-RETRY-OFF")) {
                        $PCHRetryOn = $false
                    }
                }
                if ($NeedsRetry) {
                    Write-Warning "Detected PCH memory error. Automatically retrying..."
                    continue
                }
                exit $LASTEXITCODE
            } else {
                break
            }
        }
        
        exit $LastExitCode
    } finally {
        Pop-Location
    }
}
