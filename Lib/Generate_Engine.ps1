param(
    # The distribution name, as it appears in BuildConfig.json.
    [Parameter(Mandatory = $true)][string] $Distribution,
    # The path to output the .gitlab-ci.yml file to.
    [Parameter(Mandatory = $true)][string] $GitLabYamlPath,
    # The prefix for the GitLab agent tag for running builds. If this is "abc", then jobs will be defined
    # to use the tags abc-windows and abc-mac.
    [Parameter(Mandatory = $true)][string] $GitLabAgentTagPrefix,
    # The absolute path to shared storage on Windows. Must start with a drive letter (like X:\). Must have a trailing path.
    [Parameter(Mandatory = $true)][string] $WindowsSharedStorageAbsolutePath,
    # The absolute path to shared storage on macOS. If present, must have a trailing path. If not specified, Backblaze B2 will be used as the artifact transport.
    [Parameter(Mandatory = $false)][string] $MacSharedStorageAbsolutePath,
    # The path to the Linux toolchain on Windows.
    [string] $WindowsLinuxToolchainPath
)

. $PSScriptRoot\Internal_SetupEnvironment.ps1
. $PSScriptRoot\Internal_Convert_BuildGraphToBuildPipeline.ps1
. $PSScriptRoot\Internal_Convert_BuildPipelineToGitLab.ps1

$BuildConfig = Get-BuildConfig
$DistributionConfig = Get-Distribution $Distribution

# We map the engine to a drive letter for the duration of the run, to avoid issues
# with long paths.
$local:ShortHash = "W" + (Get-StringHash -InputString "$local:Distribution-$($null)" -Length 4)
$RealEnginePath = "$ProjectRoot\$local:ShortHash\BuildScripts\Temp\UE"
if (!(Test-Path "$RealEnginePath")) {
    New-Item -ItemType Directory -Path "$RealEnginePath" | Out-Null
}
Map-WorkingDirectory -MapDriveForShorterPaths:$true -Path $RealEnginePath -ScriptBlock {
    param([string] $Path)
    $EnginePath = $Path

    # Prepare the engine. We always do this (even on every step of GitLab)
    # because this is what clones the engine and patches it for the build.
    & "$PSScriptRoot\Prepare_Engine.ps1" `
        -Distribution $Distribution `
        -EnginePath $EnginePath

    # Patch BuildGraph. We do this before computing settings, since BuildGraphSettings_Engine.ps1
    # will invoke BuildGraph to detect all of the available platforms.
    & "$PSScriptRoot\Patch_BuildGraph.ps1" -Engine "$EnginePath"
    if ($LastExitCode -ne 0) {
        exit $LastExitCode
    }

    # Determine if the engine is Unreal Engine 5.
    $local:IsUnrealEngine5 = $false
    if ($global:IsMacOS -or $global:IsLinux) {
        $local:RunUAT = Get-Content -Raw -Path "$EnginePath/Engine/Build/BatchFiles/RunUAT.sh"
        if ($local:RunUAT.Contains("Unreal Engine AutomationTool setup script")) {
            $local:IsUnrealEngine5 = $true
        }
    }
    else {
        $local:RunUAT = Get-Content -Raw -Path "$EnginePath\Engine\Build\BatchFiles\RunUAT.bat"
        if ($local:RunUAT.Contains("SET_TURNKEY_VARIABLES")) {
            $local:IsUnrealEngine5 = $true
        }
    }

    # Compute the BuildGraph settings.
    . $PSScriptRoot\BuildGraphSettings_Engine.ps1
    $Settings = Get-BuildGraphSettingsForEngine $DistributionConfig $EnginePath $false $local:IsUnrealEngine5

    # Execute BuildGraph.
    $BuildGraphArgsArray = @()
    $BuildGraphArgsArrayWindows = @()
    $BuildGraphArgsArrayMac = @()
    $Settings.Windows.Keys | ForEach-Object {
        $BuildGraphArgsArray += "-set:$_=`"$($Settings.Windows.Item($_).Replace("__REPOSITORY_ROOT__", "$ProjectRoot").Replace("/", "\").TrimEnd("\"))`""
        $local:WindowsArg = "-set:$_=`"$($Settings.Windows.Item($_).Replace("__REPOSITORY_ROOT__", "`$((Get-Location).Path)").Replace("/", "\").TrimEnd("\"))`""
        $local:WindowsArg = $local:WindowsArg.Replace("``", "````").Replace("`"", "```"")
        $local:WindowsArg = "`"$local:WindowsArg`""
        $BuildGraphArgsArrayWindows += $local:WindowsArg
    }
    $Settings.Mac.Keys | ForEach-Object {
        $local:MacArg = "-set__$_=`"$($Settings.Mac.Item($_).Replace("__REPOSITORY_ROOT__", "`$(pwd)").Replace("\", "/"))`""
        $local:MacArg = $local:MacArg.Replace("\", "\\\").Replace("`"", "\`"")
        $local:MacArg = "`"$local:MacArg`""
        $BuildGraphArgsArrayMac += $local:MacArg
    }
    $BuildGraphArgsStringWindows = $BuildGraphArgsArrayWindows -join " "
    $BuildGraphArgsStringMac = $BuildGraphArgsArrayMac -join " "
    $env:IsBuildMachine = "1"
    $Targets = @()
    if ($DistributionConfig.Build.EditorPlatforms -eq $null -or
        $DistributionConfig.Build.EditorPlatforms.Length -eq 0) {
        $Targets = @("Make Installed Build Win64")
    }
    else {
        if ($DistributionConfig.Build.EditorPlatforms.Contains("Win64")) {
            $Targets += "Make Installed Build Win64"
        }
        if ($DistributionConfig.Build.EditorPlatforms.Contains("Mac")) {
            $Targets += "Make Installed Build Mac"
        }
        if ($DistributionConfig.Build.EditorPlatforms.Contains("Linux")) {
            $Targets += "Make Installed Build Linux"
        }
    }
    $Targets = $Targets -join "+"
    & "$PSScriptRoot\Internal_RunUAT.ps1" -UATEnginePath "$EnginePath" BuildGraph -Script="$EnginePath\Engine\Build\InstalledEngineBuild.xml" -Target="$Targets" $BuildGraphArgsArray -Export="$PSScriptRoot\..\Temp\BuildGraphExport.json" -noP4
    $env:IsBuildMachine = "0"
    if ($LastExitCode -eq 0) {
        $local:BuildGraphEnvironment = [BuildGraphEnvironment]@{
            Engine        = $null;
            IsEngineBuild = $true;
            # todo: This needs to change based on the build server.
            PipelineId    = $env:CI_PIPELINE_ID;
            Windows       = [WindowsEnvironment]@{
                ProjectRoot               = $null;
                BuildGraphSettings        = $BuildGraphArgsStringWindows;
                SharedStorageAbsolutePath = $WindowsSharedStorageAbsolutePath;
                LinuxToolchainPath        = $WindowsLinuxToolchainPath;
                MapDriveForShorterPaths   = $true;
            };
            Mac           = if ($null -eq $MacSharedStorageAbsolutePath -or "" -eq $MacSharedStorageAbsolutePath) {
                [MacEnvironment]@{
                    ProjectRoot               = $null;
                    BuildGraphSettings        = $BuildGraphArgsStringMac;
                    MacEnginePathOverride     = $null;
                    SharedStorageAbsolutePath = $null;
                    ArtifactTransport         = [MacArtifactTransport]::BackblazeB2;
                };
            }
            else {
                [MacEnvironment]@{
                    ProjectRoot               = $null;
                    BuildGraphSettings        = $BuildGraphArgsStringMac;
                    MacEnginePathOverride     = $null;
                    SharedStorageAbsolutePath = $MacSharedStorageAbsolutePath;
                    ArtifactTransport         = [MacArtifactTransport]::Direct;
                };
            }
        }
        $local:BuildPipeline = Convert-BuildGraphToBuildPipeline `
            -Distribution $Distribution `
            -DistributionConfig $DistributionConfig `
            -BuildGraph (Get-Content -Raw -Path "$PSScriptRoot\..\Temp\BuildGraphExport.json" | ConvertFrom-Json) `
            -Environment $local:BuildGraphEnvironment `
            -IsUnrealEngine5 $local:IsUnrealEngine5

        # Modify the build steps for the "Make Installed Build" jobs. We have to
        # insert the deployment steps manually, since we're not in control of
        # the engine's BuildGraph.
        if ($DistributionConfig.Deployment -ne $null) {
            foreach ($Deployment in $DistributionConfig.Deployment) {
                if ($Deployment.Type -ne "LocalCopy" -or
                    ($Deployment.Platform -ne "Win64" -and $Deployment.Platform -ne "Mac" -and $Deployment.Platform -ne "Linux") -or
                    $Deployment.Target -eq $null) {
                    continue
                }

                # Scan for relevant jobs
                $local:ShortHash = "W" + (Get-StringHash -InputString "$Distribution-" -Length 4)
                foreach ($Job in $local:BuildPipeline.Jobs) {
                    if ($Job.Name -eq "Make Installed Build $($Deployment.Platform)") {
                        if ($Job.Platform -eq "Win64") {
                            Write-Output "Attaching Windows deployment script to $($Job.Name)"
                            $Job.Script = $Job.Script.Replace("exit `$LastExitCode # Build exit", "")
                            $Job.Script += @"
if (`$LastExitCode -ne 0) {
    Write-Output "Exiting before copy, because BuildGraph exited with code `$LastExitCode."
    exit `$LastExitCode
}
Write-Output "Copying to $($Deployment.Target) ..."
if (!(Test-Path "$($Deployment.Target)")) {
    New-Item -ItemType Directory "$($Deployment.Target)"
}
robocopy "`$((Get-Location).Path)\$local:ShortHash\BuildScripts\Temp\UE\LocalBuilds\Engine\Windows\" /MIR /NP "$($Deployment.Target)"
exit 0
"@
                        }
                        elseif ($Job.Platform -eq "Mac") {
                            Write-Output "Attaching macOS deployment script to $($Job.Name)"
                            $Job.Script += @"

echo "Copying to $($Deployment.Target) ..."
if [ ! -d "$($Deployment.Target)" ]; then
    mkdir -pv "$($Deployment.Target)"
fi
rsync --progress -a "`$(pwd)/$local:ShortHash/BuildScripts/Temp/UE/LocalBuilds/Engine/Mac/" "$($Deployment.Target)

"@
                        }
                    }
                }
            }
        }

        Write-Output "GitLab agent tag prefix: $GitLabAgentTagPrefix"

        $local:GitLabYaml = Convert-BuildPipelineToGitLab `
            -BuildPipeline $local:BuildPipeline `
            -AgentTagPrefix "$GitLabAgentTagPrefix"
        Set-Content -Path $GitLabYamlPath -Value $local:GitLabYaml
    }
    exit $LastExitCode
}